<?php

class ContactController extends BaseController
{

    // Show the contact form
    public function getContactForm()
    {
        // Traveltek has redirected the customer because they have not logged in
        if(Input::get('tt_error'))
        {
            return View::make('v.contact.form')
                ->with('redirect_reason', 'You have been redirected to this page because you attempted to visit the holiday search page without logging in. If you have registered, please click the <a href="/">Login</a> link at the top of this page and sign in with your information. If you have not registered, fill out your information in the boxes below to send a message to British Travel Agency and they will respond to you with information about this service.')
                ->with('defined', "I am looking for more information about the advantage subscription.<br>");
        }

        return View::make('v.contact.form');
    }

    public function postContactForm()
    {

        $validator = Validator::make(Input::all(), array(
            'name'          => 'required',
            'telephone'     => 'required',
            'email'         => 'required|email'
        ));

        if($validator->fails())
        {
            // Redirect back to the account creation page
            return Redirect::route('contact-form')
                -> withErrors($validator)
                -> withInput();
        }
        else
        {

            $name       = Input::get('name');
            $telephone  = Input::get('telephone');
            $email      = Input::get('email');
            $question   = Input::get('question');
            $detail     = Input::get('detail');
            $ip         = Input::get('ip');

            $data_lookup = User::where('email', '=', $email)->get()->first();

            $data = array(
                'name'          => $name,
                'telephone'     => $telephone,
                'email'         => $email,
                'question'      => $question,
                'detail'        => $detail,
                'ip'            => $ip,
                'data_lookup'   => $data_lookup
            );

            // Send email
            Mail::send('v.contact.external.mail.form', array('name' => $name, 'telephone' => $telephone, 'email' => $email, 'question' => $question, 'detail' => $detail, 'ip' => $ip, 'db' => $data_lookup), function($message) use ($data)
            {
                $message->to("hello@britishtravelagency.co.uk", "British Travel Agency")->subject('Contact Form Submission');
                $message->from("gds@ajrtm.com", "British Travel Agency");
            });

            // Return back to the login page with a success message
            return Redirect::route('account-login')
                -> with('global', 'The form was submitted and an agent will contact you shortly');
        }
    }
}