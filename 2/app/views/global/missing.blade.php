<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>British Travel :: Error 404</title>
        @include('global.meta.header')
    </head>
    <body>
        @include('global.include.en-US.navigation')
            <div class="container">
                <div class="row title error">
                    <div class="col-md-12">
                        <h1>404</h1>
                        <p><b>No Ma'am!</b> You won't find the secret to our squirrel friendly coffee here.<br>It seems you have encountered an error while browsing this site, you tried to look up a page that may have been removed, or may have never been here.</p>
                        <a href="javascript:history.back();">Go back to the previous page</a> <span style="margin: 0 15px; font-weight: bold;">:</span> <a href="/">Go to our home page</a>
                    </div>
                </div>
            </div>
        @include('global.meta.footer')
    </body>
</html>