<?php

$redirect = function ()
{
    Route::get('/', function()
    {
        return Redirect::to("http://status.britishtravelagency.co.uk");
    });
};

$login = function ()
{
    // Unauthenticated group
    Route::group(array('before' => 'guest'), function()
    {
        // CSRF protection group
        Route::group(array('before' => 'csrf'), function()
        {
            // Sign in (POST)
            Route::post('/account/login', array(
                'as' => 'account-login-post',
                'uses' => 'AccountController@postLogin'
            ));

            // Contact form (POST)
            Route::post('/contact/process', array(
                'as'    => 'contact-form-post',
                'uses'  => 'ContactController@postContactForm'
            ));
        });

        // Sign in (GET)
        Route::get('/', array(
            'as'    => 'account-login',
            'uses'  => 'AccountController@getLogin'
        ));

        // Contact form (GET)
        Route::get('/contact', array(
            'as'    => 'contact-form',
            'uses'  => 'ContactController@getContactForm'
        ));

        // Activate account (GET)
        Route::get('/account/activate/{code}', array(
            'as'    => 'account-activate',
            'uses'  => 'AccountController@getActivate'
        ));

    });
};

Route::group(array('domain' => 'rps.britishtravelagency.co.uk'), $login);



$agent = function ()
{
    // CSRF protection group
    Route::group(array('before' => 'csrf'), function()
    {

        // Create account (POST)
        Route::post('/account/create', array(
            'as'    => 'account-create-post',
            'uses'  => 'AccountController@postCreate'
        ));

    });

    // Sign in (GET)
    Route::get('/', array(
        'as'    => 'account-create',
        'uses'  => 'AccountController@getCreate'
    ));
};

Route::group(array('domain' => 'rpsa.britishtravelagency.co.uk'), $agent);

Route::group(array('domain' => 'idsm.btagency.co.uk'), function()
{
    Route::get('/', function()
    {
        return View::make('v.account.idsm-moved');
    });
});