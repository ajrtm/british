<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US" prefix="og: http://ogp.me/ns#">
    <head>
        <title>British Travel :: Contact</title>
        @include('global.meta.header')
    </head>
    <body>
        @include('global.include.en-US.navigation')
            <div class="container">
                <div class="row title">
                    <div class="col-md-12">
                        <h1>Get in touch with British Travel</h1>
                        @if(isset($redirect_reason))
                            <p class="text-danger">{{ $redirect_reason }}</p>
                        @endif
                    </div>
                </div>
                <div class="row">
                    <form action="{{ URL::route('contact-form-post') }}" method="post">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="name">
                                    Name
                                    @if($errors->has('name'))
                                        <p class="text-danger">{{ $errors->first('name') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="name" placeholder="John Appleseed" {{ (Input::old('name')) ? 'value="' . Input::old('name') . '"' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="telephone">
                                    Telephone Number
                                    @if($errors->has('telephone'))
                                        <p class="text-danger">{{ $errors->first('telephone') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="telephone" placeholder="01255 719819" {{ (Input::old('telephone')) ? 'value="' . Input::old('telephone') . '"' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="email">
                                    Email Address
                                    @if($errors->has('email'))
                                        <p class="text-danger">{{ $errors->first('email') }}</p>
                                    @endif
                                </label>
                                <input type="email" class="form-control input-lg" name="email" placeholder="john@appleseed.com" {{ (Input::old('email')) ? 'value="' . Input::old('email') . '"' : '' }}>
                            </div>
                            @if(!isset($defined))
                            <div class="form-group">
                                <label for="question">
                                    What is your question
                                    @if($errors->has('question'))
                                        <p class="text-danger">{{ $errors->first('question') }}</p>
                                    @endif
                                </label>
                                <input type="text" class="form-control input-lg" name="question" placeholder="I lost my password" {{ (Input::old('question')) ? 'value="' . Input::old('question') . '"' : '' }}>
                            </div>
                            <div class="form-group">
                                <label for="detail">
                                    More Information
                                    @if($errors->has('detail'))
                                        <p class="text-danger">{{ $errors->first('detail') }}</p>
                                    @endif
                                </label>
                                <textarea rows="5" class="form-control input-lg" name="detail" placeholder="Tell us more about your question. The more information you can give us, the better we can help you!" {{ (Input::old('detail')) ? 'value="' . Input::old('detail') . '"' : '' }}></textarea>
                            </div>
                            @endif
                        </div>
                        <div class="col-md-12">
                            <address>
                                <b>British Travel Agency</b><br />
                                C.C. Fañabe Plaza 223<br />
                                Avda. de Bruselas<br />
                                Costa Adeje<br />
                                Tenerife 38670
                            </address>
                            U.K. Number <a href="tel:004402081447031">020 8144 7031</a><br />
                            European Number <a href="tel:0034922702350">+34 922 702 350</a><br />
                            <br />
                            Email: <b>info@britishtravelagency.co.uk</b>
                        </div>
                        <div class="col-md-12">
                            <p>It may take up to 4 business days to respond to your request, if your request is truly critical, please contact British Travel Agency by telephone.</p>
                            <button type="submit" style="margin: 25px 0;" class="btn btn-success btn-lg btn-block">Send</button>
                        </div>
                        <input type="hidden" name="ip" value="{{ $_SERVER['REMOTE_ADDR'] }}">
                        {{ Form::token() }}
                    </form>
                </div>
            </div>
        @include('global.meta.footer')
    </body>
</html>