<font face="lucida grande" color="#141414" size="3">
Hello {{ $name }},<br><br>

Please activate your account using the following link.<br><br>

- - - - - -<br><br>
<a href="{{ $link }}" target="_blank">{{ $link }}</a><br><br>
<i>Until you activate your account you may not use the platform. Additionally, you may contact our support team by email at gds@ajrtm.com, and they will personally update your information, reset your password and activate your account.</i><br><br>
- - - - - -<br><br>
When you sign in, please use <b><font color="black">{{ $id }}</font></b> as your customer number.<br><br>
Use <b><font color="black">{{ $email }}</font></b> as your email address<br>
Use <b><font color="black">{{ $pwd }}</font></b> as your password<br>
After activating your account, you can access the platform by clicking the signin/login link at the top of our website, www.britishtravelagency.co.uk.<br><br>
(YYYY-MM-DD)<br><br>
Your subscription will <u>start</u> on {{ $start }}<br>
Your subscription will <u>end</u> on {{ $end }}<br><br>
For help and support, please visit <a href="http://www.britishtravelagency.co.uk" target="_blank">www.britishtravelagency.co.uk</a>, you can call us on our UK number, which for your record is <b>020 8144 7031</b>. <br>
Finally, you can visit <a href="http://status.britishtravelagency.co.uk/">status.britishtravelagency.co.uk</a> to check if there are any known issues if you're having problems using the service. <br>
If you need to report a problem, you can do so by visiting <a href="http://login.britishtravelagency.co.uk/contact">login.britishtravelagency.co.uk/contact</a>, or by email or telephone.
<br>
<br>
<br>
Thank you for your support, and we look forward to doing business with you. All the best,<br>
<br>
British Travel Agency<br>
C.C Fanabe Plaza 223<br>
Avda. de Bruselas<br>
Costa Adeje<br>
Tenerife 38670<br>
<br>
info@britishtravelagency.co.uk<br>
+34 922-702-350
</font>