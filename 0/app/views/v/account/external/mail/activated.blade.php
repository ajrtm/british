<font face="lucida grande" color="#141414" size="3">
British Travel Team,<br><br>

An account was activated by clicking an activation link contained within an email.<br><br>

- - - - - -<br><br>
<b>Name:</b>&nbsp;{{ $first_name }} {{ $last_name }} <br>
<b>Email:</b>&nbsp;{{ $email }} <br>
<b>Customer Number:</b>&nbsp;{{ $username }} <br>
<b>Password:</b>&nbsp;{{ $pwd }} <br> <br><br>
- - - - - -<br><br>