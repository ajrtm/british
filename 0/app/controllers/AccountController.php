<?php

class AccountController extends BaseController
{

    public function getLogin()
    {
        return View::make('v.account.login');
    }

    public function postLogin()
    {
        // Error message array
        $messages = array(
            'username.required'     => 'The customer number is required.'
        );

        $validator = Validator::make(Input::all(), array(
            'email'                     => 'required|email',
            'username'                  => 'required',
            'password'                  => 'required'

        ), $messages);

        if($validator->fails())
        {
            // Redirect back to the login page
            return Redirect::route('account-login')
                -> withErrors($validator)
                -> withInput();
        }
        else
        {
            // Attempt user login
            $auth = Auth::attempt(array(
                'username'          => strtoupper(Input::get('username')),
                'email'             => Input::get('email'),
                'password'          => Input::get('password'),
                'active'            => 1
            ));

            if($auth)
            {
                // add to counter
                $user = Auth::user();
                $user->count = $user->count + 1;
                $user->save();

                // Redirect to platform
                return Redirect::to('http://search.btagency.co.uk');
            }

        }

        return Redirect::route('account-login')
            -> with('global', 'The information provided was not valid and you have not been authenticated.');

    }

    public function getCreate()
    {
        // return the view with the next customer number to be added
        return View::make('v.account.create');
    }

    public function postCreate()
    {

        $aUID = Input::get('aUID');
        $aPWD = Input::get('aPWD');

        if(Config::get('authorized.bta.username') != $aUID || Config::get('authorized.bta.password') != $aPWD) {
            return Redirect::route('account-create')->with('global', 'AGENT NOT AUTHORIZED');
        }

        // Error message array
        $messages = array(
            'username.required'     => 'The customer number field is required.',
            'username.unique'       => 'The customer number has already been taken.'
        );

        $validator = Validator::make(Input::all(), array(
            'email'                     => 'required|max:50|email|unique:users',
            'username'                  => 'required|min:4|unique:users',
            'first_name'                => 'required',
            'last_name'                 => 'required',
            'subscription_start'        => 'required|numeric',
            'subscription_end'          => 'required|numeric'

        ), $messages);

        if($validator->fails())
        {
            // Redirect back to the account creation page
            return Redirect::route('account-create')
                -> withErrors($validator)
                -> withInput();
        }
        else
        {
            // Passed user data
            $email      = Input::get('email');
            $username   = strtoupper(Input::get('username'));
            $password   = str_random(10);
            $pwd        = $password;

            // Personal data
            $first_name = ucfirst(Input::get('first_name'));
            $last_name  = ucfirst(Input::get('last_name'));

            // calculate subscription period
            $start_delay = strtotime('+' . Input::get('subscription_start') . ' day', strtotime(date('Y-m-d')));
            $end_date    = strtotime('+' . Input::get('subscription_end') . ' year', strtotime(date('Y-m-d')));

            $subscription_start = date('Y-m-d', $start_delay);
            $subscription_end   = date('Y-m-d', $end_date);

            // Activation code
            $code       = str_random(60);

            // Create user account
            $user = User::create(array(
                'last_name'             => $last_name,
                'first_name'            => $first_name,
                'email'                 => $email,
                'username'              => $username,
                'pwd'                   => $pwd,
                'password'              => Hash::make($password),
                'code'                  => $code,
                'active'                => 0,
                'subscription_start'    => $subscription_start,
                'subscription_end'      => $subscription_end
            ));

            if($user)
            {
                // Send confirmation email
                Mail::send('v.account.external.mail.activate', array('link' => URL::route('account-activate', $code), 'name' => $first_name, 'id' => $username, 'start' => $subscription_start, 'end' => $subscription_end, 'pwd' => $pwd, 'email' => $email), function($message) use ($user)
                {
                    $message->to($user->email, $user->first_name)->subject('Activate your account');
                    $message->from('gds@ajrtm.com', 'British Travel Agency');
                });

                // Return back to the login page with a success message
                return Redirect::route('account-create')
                    -> with('global', 'The account was created and an email was sent to the specified address for confirmation.');
            }
        }
    }

    public function getActivate($code)
    {
        $user = User::where('code', '=', $code)->where('active', '=', 0);

        if($user->count())
        {
            $user = $user->first();

            // Activate the user account
            $user->active   = 1;
            $user->code     = '';

            $last_name             = $user->last_name;
            $first_name            = $user->first_name;
            $email                 = $user->email;
            $username              = $user->username;
            $pwd                   = $user->pwd;

            // Get user information
            $data = array(
                'last_name'             => $last_name,
                'first_name'            => $first_name,
                'email'                 => $email,
                'username'              => $username,
                'pwd'                   => $pwd
            );

            // Save the activated user account
            if($user->save())
            {
                // Send confirmation email
                Mail::send('v.account.external.mail.activated', array('first_name' => $first_name, 'last_name' => $last_name, 'username' => $username, 'email' => $email, 'pwd' => $pwd), function($message) use ($data)
                {
                    $message->to("hello@britishtravelagency.co.uk", "British Travel Agency")->subject('Account Activated');
                    $message->from("gds@ajrtm.com", "British Travel Agency");
                });

                return Redirect::route('account-login')
                    -> with('global', 'The account was activated, you may sign in below.');
            }
        }

        return Redirect::route('account-login')
            -> with('global', 'The account activation code is invalid.');

    }

}