#!/bin/bash

cd ~/Desktop/Development/Source.old/British
git fetch -v
git add --all --verbose
git commit --allow-empty-message -m ''
git push -v
nodes=( robert theo )

for ((i = 0; i < ${#nodes[@]}; i++)) do

    printf "\n\n\n"
    echo ${nodes[$i]}
    printf "===============================================\n"

    ssh ${nodes[$i]} '
        apt-get autoremove
        apt-get -y upgrade

        cd /var/www/british

        chmod -R 777 /var/www/british/0/app/storage

        git fetch
        git checkout master --force
        git pull -vf

        cd /etc/apache2/sites-available

        cp -f /var/www/british/british.conf .
        rm ../sites-enabled/british.conf

        sudo ln /etc/apache2/sites-available/british.conf /etc/apache2/sites-enabled/british.conf

        service apache2 restart

        exit
    '

done

printf "\n\n\n\n - - - - - - - - - - - - - - - - - - - - - - - - - - - - \n\n\n\n"

git status
